# city-bike-backend
    - The project is using GraphQL deliver data from backend and MongoDB for database

# Features 

## Station
    - Can get station list with pagination, sorting, searching
    - Can get single station

## Journey
    - Can get journey list with pagination, sorting, searching

## Data upload 
    - Can handle csv file from frontend and save documents to db
