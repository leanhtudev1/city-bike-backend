import { getJourneys } from '../controllers/journeyControllers';

const express = require('express');
const router = express.Router();

router.get('/list', getJourneys);

export default router;
