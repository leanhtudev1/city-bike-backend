import { getStation, getStations } from '../controllers/stationControllers';

const express = require('express');
const router = express.Router();

router.get('/list', getStations);
router.get('/:id', getStation);

export default router;
