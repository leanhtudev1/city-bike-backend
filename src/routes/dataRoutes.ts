const express = require('express');
import multer from 'multer';
const router = express.Router();
import { uploadData } from './../controllers/dataControllers';

const multerStorage = multer.diskStorage({
  destination: (req: any, file: any, cb: any) => {
    console.log(__dirname + '/../uploads', 'ua');
    cb(null, __dirname + '/../uploads');
  },
  filename: (req: any, file: any, cb: any) => {
    cb(null, file.originalname || '');
  },
});
const upload = multer({
  storage: multerStorage,
});

router.post('/data/upload', upload.single('file'), uploadData);

export default router;
