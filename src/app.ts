import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { graphqlHTTP } from 'express-graphql';
import dataRouter from './routes/dataRoutes';
import stationRoutes from './routes/stationRoutes';
import journeyRoutes from './routes/journeyRoutes';
import schema from './schema/schema';

const app = express();
app.use(cors());

app.options('*', cors());

// parse application/json
app.use(bodyParser.json());
app.use(
  '/api/v1/graphql',
  // @ts-ignore
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);
app.use(express.static(__dirname + '/build'));
// Routes
app.use('/api/v1', dataRouter);
app.use('/api/v1/station', stationRoutes);
app.use('/api/v1/journey', journeyRoutes);

app.use('/', (req, res, next) => {
  res.json({ status: 200, data: 'success' });
});

export default app;
