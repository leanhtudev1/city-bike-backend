import APIFeatures from '../models/Api';
import Journey from '../models/journey';

const getJourneys = async (query: any) => {
  // const origin = new APIFeatures(Journey.find(), req.query);
  console.log(query);
  const features = new APIFeatures(
    Journey.find().populate([
      { path: 'departureStationId' },
      { path: 'returnStationId' },
    ]),
    query
  )
    .sort()
    .paginate();
  const data = await features.query;
  return {
    journeys: data,
    total: 10,
  };
};

export { getJourneys };
