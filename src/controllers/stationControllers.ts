import APIFeatures from '../models/Api';
import Journey from '../models/journey';
import Station from '../models/station';

const getStations = async (query: any) => {
  let regexQuery = {};
  if (query.searchBy && query.searchKey) {
    regexQuery = {
      [query.searchBy]: new RegExp(query.searchKey, 'i'),
    };
  }
  const origin = new APIFeatures(Station.find(regexQuery), query);
  const features = new APIFeatures(Station.find(regexQuery), query)
    .sort()
    .paginate();
  const data = await features.query;
  const originData = await origin.query;
  return {
    stations: data,
    total: Math.ceil(originData.length / query.limit),
  };
};

const getStation = async (id: string) => {
  let station = Station.findById(id);
  const doc = await station;
  let departWithJourneys = await Journey.find().where({
    departureStationId: { $eq: id },
  });

  let returnWithJourneys = await Journey.find().where({
    returnStationId: { $eq: id },
  });
  if (!doc) {
    return { station: undefined, departure: 0, return: 0 };
  }

  return {
    station: doc,
    departure: departWithJourneys.length,
    return: returnWithJourneys.length,
  };
};
export { getStations, getStation };
