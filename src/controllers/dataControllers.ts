import csvtojson from 'csvtojson/v2';
import Station from './../models/station';
import Journey from './../models/journey';
const camelize = (str: string) => {
  if (str === 'Duration (sec.)') return 'duration';
  if (str === 'Covered distance (m)') return 'distance';
  return str
    .replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    })
    .replace(/\s+/g, '');
};

const _csvToJson = async (filePath: string, fileName: string) => {
  const jsonObj = await csvtojson({ flatKeys: true }).fromFile(filePath);
  if (jsonObj) return jsonObj;
  return undefined;
};
interface Journey {
  departure: string;
  return: string;
  departureStationId: string;
  departureStationName: string;
  returnStationId: string;
  returnStationName: string;
  distance: number;
  duration: number;
}

const isEligibleJourney = (journey: Journey) => {
  return journey.duration >= 10 && journey.distance >= 10;
};

const sanitizeCsvJourney = (csvJourney: Record<string, any>) => {
  return Object.keys(csvJourney).reduce((acc, key: string) => {
    if (
      [
        'distance',
        'duration',
        'departureStationId',
        'returnStationId',
      ].includes(camelize(key))
    ) {
      acc[camelize(key)] = Number(csvJourney[key]);
    } else {
      acc[camelize(key)] = csvJourney[key];
    }
    return acc;
  }, {} as Record<string, any>);
};
const StationFieldWhiteList = [
  'ID',
  'Name',
  'Adress',
  'Kaupunki',
  'Kapasiteet',
  'x',
  'y',
];

const sanitizeCsvStation = (csvStation: Record<string, any>) => {
  return Object.keys(csvStation).reduce((acc, key: string) => {
    if (!StationFieldWhiteList.includes(key)) return acc;

    switch (key) {
      case 'ID':
        acc['id'] = Number(csvStation[key]);
        break;
      case 'Adress':
        acc['address'] = csvStation[key];
        break;

      case 'Kaupunki':
        acc['city'] = csvStation[key];
        break;

      case 'Kapasiteet':
        acc['capacity'] = Number(csvStation[key]);
        break;
      case 'x':
        acc['lat'] = Number(csvStation[key]);
        break;
      case 'y':
        acc['long'] = Number(csvStation[key]);
        break;
      default:
        acc[camelize(key)] = csvStation[key];
    }
    return acc;
  }, {} as Record<string, any>);
};
const normalizeJourneyCSVData = (
  data: Array<Record<string, any>>,
  refData: Record<string, any>
) => {
  if (!data || data?.length === 0) return [];
  return data.reduce((acc, item) => {
    const normalizedItem = sanitizeCsvJourney(item);
    if (
      isEligibleJourney(normalizedItem as Journey) &&
      refData[normalizedItem.departureStationId.toString()] &&
      refData[normalizedItem.returnStationId.toString()]
    ) {
      acc.push({
        ...normalizedItem,
        departureStationId:
          refData[normalizedItem.departureStationId.toString()],
        returnStationId: refData[normalizedItem.returnStationId.toString()],
      });
    }
    return acc;
  }, [] as Journey[]);
};

const normalizeStationCSVData = (data: Array<Record<string, any>>) => {
  if (!data || data?.length === 0) return [];
  return data.reduce((acc, item) => {
    const normalizedItem = sanitizeCsvStation(item);
    acc.push(normalizedItem);

    return acc;
  }, [] as Journey[]);
};
const resolveDataByType = async (json_data: any, fileType: string) => {
  switch (fileType) {
    case 'journey':
      const data = await Station.find().select('id _id');
      const refData = await data.reduce((acc: any, item: any) => {
        acc[item.id] = item._id;
        return acc;
      }, {});

      const journeys = normalizeJourneyCSVData(json_data || [], refData);

      return Journey.insertMany(journeys);

    case 'station':
    default:
      return Station.insertMany(normalizeStationCSVData(json_data || []));
  }
};
export const uploadData = async (req: any, res: any, next: any) => {
  const { file, query } = req;
  if (file) {
    const json_data = await _csvToJson(
      `${__dirname}/../uploads/${file.originalname}`,
      file.originalname
    );

    if (json_data) {
      const docs = await resolveDataByType(json_data, query.fileType);
      console.log(docs, 'hehe doc ne');
      return res.status(200).json({ data: 'Success' });
    }
    res.status(500).json({ data: 'Failed' });
  }
};
