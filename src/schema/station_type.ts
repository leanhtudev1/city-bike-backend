import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLList,
} from 'graphql';

export const StationType = new GraphQLObjectType({
  name: 'StationType',
  fields: () => ({
    _id: { type: GraphQLString },
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    address: { type: GraphQLString },
    city: { type: GraphQLString },
    capacity: { type: GraphQLInt },
    lat: { type: GraphQLFloat },
    long: { type: GraphQLFloat },
  }),
});


export const StationListType = new GraphQLObjectType({
  name: 'StationListType',
  fields: () => ({
    stations: { type: new GraphQLList(StationType) },
    total: { type: GraphQLInt },
  }),
});

