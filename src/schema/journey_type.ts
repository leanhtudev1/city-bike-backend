import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLFloat,
} from 'graphql';
import { StationType } from './station_type';

export const JourneyType = new GraphQLObjectType({
  name: 'JourneyType',
  fields: () => ({
    departureStationId: { type: StationType },
    returnStationId: { type: StationType },
    distance: { type: GraphQLFloat },
    duration: { type: GraphQLFloat },
    departure: { type: GraphQLFloat },
    return: { type: GraphQLFloat },
  }),
});

export const JourneyListType = new GraphQLObjectType({
  name: 'JourneyListType',
  fields: () => ({
    journeys: { type: new GraphQLList(JourneyType) },
    total: { type: GraphQLInt },
  }),
});
