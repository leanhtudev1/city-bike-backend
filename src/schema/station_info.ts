import { GraphQLObjectType, GraphQLInt } from 'graphql';
import { StationType } from './station_type';

const StationInfoType = new GraphQLObjectType({
  name: 'StationInfoType',
  fields: () => ({
    station: { type: StationType },
    departure: { type: GraphQLInt },
    return: { type: GraphQLInt },
  }),
});

export default StationInfoType;
