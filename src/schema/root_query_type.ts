const mongoose = require('mongoose');
const graphql = require('graphql');
const { GraphQLObjectType } = graphql;

import { GraphQLInt, GraphQLString } from 'graphql';
import { getStations, getStation } from '../controllers/stationControllers';
import StationInfoType from './station_info';
import { StationListType } from './station_type';
import { JourneyListType } from './journey_type';
import { getJourneys } from '../controllers/journeyControllers';

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: () => ({
    stations: {
      type: StationListType,
      args: {
        searchBy: { type: GraphQLString },
        searchKey: { type: GraphQLString },
        sortBy: { type: GraphQLString },
        sortDirection: { type: GraphQLString },
        page: { type: GraphQLInt },
        limit: { type: GraphQLInt },
      },

      async resolve(parentValue: any, args: any) {
        return getStations(args);
      },
    },
    stationInfo: {
      type: StationInfoType,
      args: {
        id: { type: GraphQLString },
      },
      async resolve(parentValue: any, args: any) {
        return getStation(args.id);
      },
    },
    journeys: {
      type: JourneyListType,
      args: {
        searchBy: { type: GraphQLString },
        searchKey: { type: GraphQLString },
        sortBy: { type: GraphQLString },
        sortDirection: { type: GraphQLString },
        page: { type: GraphQLInt },
        limit: { type: GraphQLInt },
      },

      async resolve(parentValue: any, args: any) {
        return getJourneys(args);
      },
    },
  }),
});

export default RootQuery;
