class APIFeatures {
  query: any;
  queryString: Record<string, any>;
  constructor(query: any, queryString: Record<string, any>) {
    this.query = query;
    this.queryString = queryString;
  }
  sort() {
    if (this.queryString.sortBy) {
      this.query = this.query.sort({
        [this.queryString.sortBy]: this.queryString.sortDirection || 'asc',
      });
    } else {
      this.query = this.query.sort({ ['id']: 'asc' });
    }

    return this;
  }
  paginate() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 10;
    const skip = (page - 1) * limit;

    this.query = this.query.skip(skip).limit(limit);

    return this;
  }
}
export default APIFeatures;
