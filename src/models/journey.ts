const mongoose = require('mongoose');

const journeySchema = new mongoose.Schema({
  departureStationId: {
    type: mongoose.Schema.ObjectId,
    ref: 'Station',
    required: [true, 'Journey must has departure station'],
  },
  returnStationId: {
    type: mongoose.Schema.ObjectId,
    ref: 'Station',
    required: [true, 'Journey must has return station'],
  },
  distance: {
    type: Number,
    require: [true, 'Booking must have a distance.'],
  },
  duration: {
    type: Number,
    require: [true, 'Booking must have a duration.'],
  },
  departure: {
    type: Date,
    default: Date.now(),
  },
  return: {
    type: Date,
    default: Date.now(),
  },
});

const Journey = mongoose.model('Journey', journeySchema);

export default Journey;
