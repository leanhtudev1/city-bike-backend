const mongoose = require('mongoose');

const stationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    id: {
      type: Number,
    },
    address: {
      type: String,
    },
    city: {
      type: String,
    },
    capacity: {
      type: Number,
    },
    lat: {
      type: Number,
    },
    long: {
      type: Number,
    },
    __v: { type: Number, select: false },
  },
  { versionKey: false }
);

const Station = mongoose.model('Station', stationSchema);

export default Station;
